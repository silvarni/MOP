# MOP Projet 2016 Licence Informatique
Projet du module Méthodes et Outils pour la Programmation (MOP)
Développé en java par le binôme Ervan Silver et Romain Mekarni

![Application](Application.png)

## Description 
La Direction Régionale des Affaires Culturelles a besoin d'une application pour connaître la liste des Musées et des Monuments Historiques de Franche-Comté.
Pour cela, on dispose de deux fichiers CSV, l'un contenant les musées et l'autre les monuments Historiques.

Le but de ce projet est d'écrire une application qui soit capable de lire les fichiers CSV de manière à construire une collection de point d'intérêts.

## Manuel d'utilisation
Cette application est capable de questionner la collection de point d'intérêts pour retrouver des lieux via différents filtres : la catégorie du lieu, un mot clé, une ville etc. Tous les filtres s'appliquent en temps réel sur les données pour afficher les points d'intérêts qui passent les filtres sur la carte et dans l'arbre d'accès aux détails.

Les résultats sont positionnés sur la carte en fonction de la latitude et de la longitude, sur une carte de la Franche-Comté avec des pictogrammes explicites.

## Choix d'implémentation

![Diagramme](DIAGRAM.png)

Nous avons utilisé un modèle MVC comme demandé, chaque traitement est séparé dans une classe spécialisée. 
Ainsi le modèle comprend des DAOs, la vue contient une classe par filtre et le contrôleur fait tout lui-même.

### Modèle
Le format des fichiers CSV (Comma Separated Values) est l'un des plus simples qu'on puisse imaginer. Une ligne contient plusieurs champs/colonnes séparés par des virgules. La première ligne contient les colonnes. La lecture d'un fichier CSV est réalisée en trois étapes :
* la lecture bas niveau des données dans un fichier
* la prise en compte du format des données
* et enfin l'utilisation (transformation) des données.

__Helper__ :
Pour la lecture bas niveau d'un fichier CSV en Java, on doit manipuler un objet `File`. Une fois ce fichier ouvert, on utilise des objets `FileReader` et `BufferedReader` pour simplifier les traitements par ligne du fichier.
La classe `BufferedReader` possède la méthode `readLine()` permettant de lire d'un coup une ligne.
La classe `CsvFileHelper` permet donc, à partir d'un objet `File` de lire tout un fichier CSV et de retourner les données sous la forme d'une liste de `String`

__CsvFile__ :
Cette classe instancie un fichier CSV lu et chargé en mémoire sous forme de tableau de `String` à l'aide de `CsvFileHelper`. C'est elle qui se charge de la mémoire des données, et éventuellement de les actualiser si le fichier csv correspondant est modifié.

__Utilisation via un DAO__ :
"Data Access Object" est un outil permettant de lire et interagir avec les données brutes. Dans un programme, on évite d'accéder directement aux données brutes, on préfère travailler avec des objets du domaine/métier, des collections d'objets (ici les `PointInteret`). Le DAO est l'interface d'accès à ces données, et permet de les retourner sous la forme d'une collection facilement manipulée par les autres packages. C'est le rôle des classes `CsvMuseeDao` et `CsvMonumentDao`

### Vue
Nous avons décider d'avoir une classe ```View``` qui contient les différents
utilitaires. Voici la liste de ces derniers et leur rôles :
* `Buttons` : Contient la liste des boutons (catégories)
*``Search`: Contient la bar de recherche et l'arbre de résultat correspondant
* `Description`: Un tableau à onglet pour présenter chaque résultat sélectionné

Nous avons choisi d'utiliser un `JTree` pour présenter l'ensemble des résultats
pour une raison de lisibilité et de pratique en plus de l'expérience que cela apporte.

Enfin, l'affichage des pins se fait par drawComponent et la sélection souris par un parcours de tous les 
points d'intérêts affichés. Ainsi une recherche au clique est en `O(n)`.

### Contrôleur 
Il s'agit d'une unique classe qui définit elle même qui elle contrôle dans son constructeur.

Il agit chez nous comme un filtre: désélectionner une catégorie empêche de croiser les mots clefs qui correspondent mais font partie de ladite catégorie.

Il autorise également une sélection rectangulaire sur la `Map` pour une meilleure ergonomie.

## Efficacité des algorithmes : `O(n)`
Chaque recherche parcours le fichier CSV conservé en mémoire sous forme de chaînes de caractères pour trouver les points d'intérêts qui correspondent.

Dès lors, toute recherche est en `O(n)`. Le transfert de la recherche vers l'affichage passe par un `Object.clone()` en _temps constant_.

Nous parcourons toutefois une fois ou deux l'ensemble des résultats dans le cas d'une sélection rectangulaire et dans la construction de l'arbre de résultat.

## Fonctionnalités
Pour fournir une bonne expérience de recherche, on a choisit de proposer à l'utilisateur plusieurs filtres de sélection (mot clé, catégories, et sélection sur carte). L'affichage des résultats étant aussi important, on propose deux manières. La première, dans un arbre résultat qui regroupe par catégories et qui est ouvert automatiquement s'il y a moins de 15 éléments. Il y a également un affichage par onglets des résultats pour que l'utilisateur puisse accéder à toutes les informations d'une entrée de la base de données (et copier coller etc.).

## Cas de test
Nous avons testé chaque élément séparément mais tout semble fonctionner ensemble.

Le cas type d'utilisation envisagé est la recherche de 'Halle' qui ne sont pas des fermes puis sélectionner les plus au nord.


## Bugs 
* Pendant le développement, il a été observé que quelques lignes des fichiers CSV fournis ne sont pas complètes et lancent des exceptions. Elles sont traitées, et chaque ligne qui ne correspond pas aux colonnes du fichier CSV sont ignorées et signalées.
* Le rectangle de sélection peut mal s'afficher en activant d'autres contrôles. Cela reste purement visuel.
- L'arbre des résultats ne tient plus en compte la sélection rectangulaire si d'autres filtres de données sont modifiés.

## Améliorations
- Déplacer quelques fonctions comme la mise à jour des données en fonction des filtres. `View` est sensé le faire à partir d'une fonction `update`
- Ajouter quelques filtres supplémentaires comme la sélection par commune (ou toutes autres colonnes des fichiers CSV traités)

## Conclusion
On a commencé un peu vite le code source sans réfléchir assez à l'application dans l'ensemble ce qui cause quelques désordres dans le code. Avec plus de temps, on aurait bien reformaté pour une structure plus simplifiée et cohérente. Mais dans l'ensemble, l'application créée atteint les objectifs demandés et avec quelques fonctionnalités supplémentaires. Le plus important a été pour nous l'ergonomie, car quel est l'intérêt de disposer d'une grande base de données si on ne peut pas intuitivement récupérer les informations qui nous intéressent.

