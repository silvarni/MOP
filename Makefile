CXX=javac
CXXFLAGS=-encoding UTF-8
EXECFLAGS=-cp ${LIB_DIR}

SRC_DIR=src
SRC=$(wildcard ${SRC_DIR}/*.java ${SRC_DIR}/*/*.java )

LIB_DIR=lib
LIB_SRC=${wildcard ${LIB_DIR}/*.java}

JAR:=MOP
ENTRY_POINT=Application/MOP

all:  prog 
	@echo -e '--- Compilation Done !\n'

jar: prog
	jar -cfmv ${JAR}.jar MANIFEST.MF -C ${LIB_DIR} .
	
prog: ${SRC}
	@echo -e '--- Compiling program\n'
	${CXX} ${CXXFLAGS} -d ${LIB_DIR} -cp ${LIB_DIR} $^

clean:
	rm -rf ${LIB_DIR}/*.class

mrproper: clean 
	rm -rf ${LIB_DIR}/*

run: all 
	@echo -e '--- Running the program.\n'
	java ${EXECFLAGS} ${ENTRY_POINT}

runjar: jar
	java -jar ${JAR}.jar
