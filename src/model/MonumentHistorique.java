package Model;
/**
 * MonumentHistorique
 */
public class MonumentHistorique extends PointInteret
{
	// --- public
	public MonumentHistorique(double latitude, double longitude,
							  String reference, int departement, String commune,
							  int insee, String categorie, String designation,
							  String proprietaire, String description,
							  String protection, String auteur, String date)
	{
		super(latitude, longitude, commune, insee, description);

		m_reference	= reference;
		m_departement  = departement;
		m_categorie	= categorie;
		m_designation  = designation;
		m_proprietaire = proprietaire;
		m_protection   = protection;
		m_auteur	   = auteur;
		m_date		   = date;
	}

	/// Builds a description from a Monument Historique
	@Override public String get_description()
	{
		return "Categorie\t" + m_categorie + "\nCommune\t\t" + m_commune +
			"\nInsee\t\t" + m_insee + "\nPosition\t(" + m_position.m_latitude +
			"N, " + m_position.m_longitude + "W)"
			+ "\nReference\t" + m_reference + "\nDepartement\t" +
			m_departement + "\nDesignation\t" + m_designation +
			"\nProprietaire\t" + m_proprietaire + "\nDate Protection\t" +
			m_protection + "\nAuteur(s)\t" + m_auteur + "\nDate\t\t" + m_date +
			"\nDescriptif:\n\t" + m_descriptif;
	}

	/// Builds a short description of a MonumentHistorique
	@Override public String toString()
	{
		return m_categorie + ": " + m_designation + "(" + m_date + ")";
	}

	// --- private
	private String m_reference;
	private int	m_departement;
	private String m_designation;
	private String m_proprietaire;
	private String m_protection;
	private String m_auteur;
	private String m_date;
}
