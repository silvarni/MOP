
package Model;
import java.util.*;

/**
 * Model
 */
public class Model
{
	// --- public
	public Model()
	{
		m_csv_musee_dao	= new CsvMuseeDao("/assets/museums.csv");
		m_csv_monument_dao = new CsvMonumentDao("/assets/fc_historical.csv");
		m_interets		   = new HashMap< String, ArrayList< PointInteret > >();
		m_interets.put("musee", m_csv_musee_dao.find_all_musees());
		categories = m_csv_monument_dao.find_all_categories();

		for (String categorie : categories)
			m_interets.put(categorie, m_csv_monument_dao.find_by_column(
										  "catégorie", categorie));
		categories.add("musee");
		Collections.sort(categories);
	}

	/**
	 * Adds a MonumentHistorique into the saved {@link PointInteret}
	 **/
	public void add_interest_point(MonumentHistorique pi)
	{
		if (m_interets.get(pi.get_categorie()) == null)
		{
			m_interets.put(pi.get_categorie(), new ArrayList< PointInteret >());
		}
		m_interets.get(pi.get_categorie()).add(pi);
		System.out.println("[MAP] Added a MonumentHistorique.");
	}

	/**
	 * @return the whole collection of PointInteret sorted by categories into an
	 *hashmap
	 **/
	public HashMap< String, ArrayList< PointInteret > > get_collection()
	{
		return m_interets;
	}

	/// Simple getter
	public CsvMonumentDao get_monument_dao() { return m_csv_monument_dao; }

	/// Simple getter
	public CsvMuseeDao get_musee_dao() { return m_csv_musee_dao; }

	/// Static list of categories mainly used for GUI
	static public ArrayList< String > categories;

	// --- private
	/// List of PointInteret sorted by category in a hashmap
	private HashMap< String, ArrayList< PointInteret > > m_interets;

	private CsvMuseeDao m_csv_musee_dao; /// The Data Access Object for museums

	/// The Data Access Object for MonumentHistorique
	private CsvMonumentDao m_csv_monument_dao;
}