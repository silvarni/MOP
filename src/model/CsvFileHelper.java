package Model;

import java.io.*;
import java.util.*;

/**
 * CsvFileHelper
 */
public class CsvFileHelper
{
	// --- public

	/**
	 * Read a CSV file regardless the encoding format
	 **/
	public static ArrayList< String > read_file(String file_name)
	{
		if (file_name == null)
			throw new IllegalArgumentException("File cannot be null");

		final ArrayList< String > result = new ArrayList< String >();

		InputStreamReader br  = null;
		BufferedReader	brr = null;
		try
		{
			br = new InputStreamReader(
				CsvFileHelper.class.getResourceAsStream(file_name), "UTF-8");
			brr = new BufferedReader(br);
			for (String line = brr.readLine(); line != null;
				 line		 = brr.readLine())
				result.add(line);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (br != null)
			{
				try
				{
					br.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		return result;
	}
}