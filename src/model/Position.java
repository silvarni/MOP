package Model;
/**
 * Positionable
 * We didn't know Point existed.
 * Represent a geolocalized position (latitude + longitude)
 */
public class Position
{
	// --- public
	public Position(double latitude, double longitude)
	{
		m_latitude  = latitude;
		m_longitude = longitude;
	}

	public String toString()
	{
		return "(" + m_latitude + "N " + m_longitude + "W)";
	}

	public double m_latitude;
	public double m_longitude;
}
