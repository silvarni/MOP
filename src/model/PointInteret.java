package Model;

/**
 * PointInteret
 */
public class PointInteret implements Comparable
{
	// --- public
	public PointInteret(double latitude, double longitude, String commune,
						int insee, String descriptif)
	{
		m_position   = new Position(latitude, longitude);
		m_commune	= commune;
		m_insee		 = insee;
		m_descriptif = descriptif;
		m_categorie  = "";
	}

	public Position get_position() { return m_position; }

	public String get_description()
	{
		return "Position : (" + m_position.m_latitude + "N, " +
			m_position.m_longitude + "W)\nCommune: " + m_commune + "\nInsee: " +
			m_insee + "\nDescription:\n\t" + m_descriptif;
	}

	/// Compare a PointInteret by its short description (a string compare)
	public int compareTo(Object other)
	{
		PointInteret pi = (PointInteret)(other);
		return toString().compareToIgnoreCase(pi.toString());
	}

	public String get_categorie() { return m_categorie; }

	// --- protected
	protected Position m_position;
	protected String   m_commune;
	protected int	  m_insee;
	protected String   m_descriptif;
	protected String   m_categorie;
}
