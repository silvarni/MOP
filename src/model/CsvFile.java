package Model;

import java.awt.List;
import java.util.ArrayList;
import java.util.*;
import java.io.*;

/**
 * CsvFile
 */
public class CsvFile
{
	// --- public
	public CsvFile(String file_name)
	{
		lines = CsvFileHelper.read_file(file_name);
		data  = new ArrayList< String[] >(lines.size());
		String sep = new Character(SEPARATOR).toString();
		for (String line : lines)
		{
			String[] one_data = line.split(sep);
			data.add(one_data);
		}
	}

	/**
	 * Return the whole data
	 * @return The datas loaded
	 **/
	public ArrayList< String[] > get_data() { return data; }

	/**
	 * Return all the columns in the CSV read from the first row
	 * @return The list of column name
	 **/
	public ArrayList< String > find_all_columns()
	{
		final ArrayList< String > columns = new ArrayList< String >();

		for (String column : data.get(0))
			columns.add(column.toLowerCase());
		return columns;
	}

	public final static char SEPARATOR = '\t'; /// Separator in the CSV

	// --- PRIVATE
	private File				  file;  /// The file we read
	private ArrayList< String >   lines; /// The lines read
	private ArrayList< String[] > data;  /// The data = lines splited
}