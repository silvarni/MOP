package Model;

import java.awt.List;
import java.util.ArrayList;

/**
 * CsvMonumentDao
 */
public class CsvMonumentDao
{
	// --- public
	public CsvMonumentDao(String file_name)
	{
		this.csvFile = new CsvFile(file_name);
	}

	/**
	 * Returns the list of monument
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_all_monuments()
	{
		final ArrayList< PointInteret > monuments =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		for (int i = 1; i < data.size(); i++)
		{
			PointInteret monument = tabToMonument(data.get(i));
			if (monument != null)
				monuments.add(monument);
		}
		return monuments;
	}

	/**
	 * Reads the memorized lines to find out all the categories (values in a
	 *column)
	 * @return An arrayList of String
	 **/
	public ArrayList< String > find_all_categories()
	{
		final ArrayList< String > categories = new ArrayList< String >();
		final ArrayList< String[] > data	 = csvFile.get_data();

		for (int i = 1; i < data.size(); i++)
		{
			String categorie = new String(data.get(i)[6]);
			if (!categories.contains(categorie))
				categories.add(categorie);
		}
		return categories;
	}

	/**
	 * Returns the list of monument matching a keywork in a specific column
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_by_column(String column,
													String keyword)
	{
		final ArrayList< PointInteret > monuments =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		int index_column = -1;
		for (int i = 0; i < data.get(0).length; i++)
		{
			if (data.get(0)[i].compareToIgnoreCase(column) == 0)
			{
				index_column = i;
				break;
			}
		}
		if (index_column == -1)
			return monuments;

		for (int i = 1; i < data.size(); i++)
		{
			if (data.get(i)[index_column].toLowerCase().contains(
					keyword.toLowerCase()))
			{
				PointInteret monument = tabToMonument(data.get(i));
				if (monument != null)
					monuments.add(monument);
			}
		}
		return monuments;
	}

	/**
	 * Returns the list of PointInteret matching a keyword
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_by_keyword(String keyword)
	{
		final ArrayList< PointInteret > monuments =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		for (int i = 1; i < data.size(); i++)
		{
			boolean match = false;
			for (String s : data.get(i))
			{
				if (s.toLowerCase().contains(keyword.toLowerCase()))
				{
					match = true;
					break;
				}
			}
			if (match)
			{
				PointInteret monument = tabToMonument(data.get(i));
				if (monument != null)
					monuments.add(monument);
			}
		}
		return monuments;
	}

	// --- private
	/**
	 * Build a MonumentHistorique from a string array
	 * @return the created MonumentHistorique
	 **/
	private MonumentHistorique tabToMonument(String[] tab)
	{
		MonumentHistorique monument = null;
		try
		{
			monument = new MonumentHistorique(
				Double.parseDouble(tab[0]), Double.parseDouble(tab[1]), tab[2],
				Integer.parseInt(tab[3]), tab[4], Integer.parseInt(tab[5]),
				tab[6], tab[7], tab[8], tab[9], tab[10], tab[11], tab[12]);
		}
		catch (Exception e)
		{
			System.out.print("\nA data entry is corrumpted, skip it : \n");
			for (String s : tab)
				System.out.print(s + "\t");
			System.out.print("\n");
			return null;
		}
		return monument;
	}

	private CsvFile csvFile; /// The CSV file analyzed
}