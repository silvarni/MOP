package Model;

import java.awt.List;
import java.util.ArrayList;

/**
 * CsvMuseeDao
 */
public class CsvMuseeDao
{
	// --- public
	public CsvMuseeDao(String file_name)
	{
		this.csvFile = new CsvFile(file_name);
	}

	/**
	 * Returns the list of museums
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_all_musees()
	{
		final ArrayList< PointInteret > musees =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		for (int i = 1; i < data.size(); i++)
		{
			PointInteret musee = tabToMusee(data.get(i));
			if (musee != null)
				musees.add(musee);
		}
		return musees;
	}

	/**
	 * Returns the list of museums matching a keyword in a specific column
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_by_column(String column,
													String keyword)
	{
		final ArrayList< PointInteret > musees =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		int index_column = -1;
		for (int i = 0; i < data.get(0).length; i++)
		{
			if (data.get(0)[i].compareToIgnoreCase(column) == 0)
			{
				index_column = i;
				break;
			}
		}
		if (index_column == -1)
			return musees;

		for (int i = 1; i < data.size(); i++)
		{
			if (data.get(i)[index_column].toLowerCase().contains(
					keyword.toLowerCase()))
			{
				PointInteret monument = tabToMusee(data.get(i));
				if (monument != null)
					musees.add(monument);
			}
		}
		return musees;
	}

	/**
	 * Returns the list of museums matching a keyword
	 * @return An arrayList of PointInteret
	 **/
	public ArrayList< PointInteret > find_by_keyword(String keyword)
	{
		final ArrayList< PointInteret > musees =
			new ArrayList< PointInteret >();

		final ArrayList< String[] > data = csvFile.get_data();

		for (int i = 1; i < data.size(); i++)
		{
			boolean match = false;
			for (String s : data.get(i))
			{
				if (s.toLowerCase().contains(keyword.toLowerCase()))
				{
					match = true;
					break;
				}
			}
			if (match)
			{
				PointInteret monument = tabToMusee(data.get(i));
				if (monument != null)
					musees.add(monument);
			}
		}
		return musees;
	}

	// --- private
	/**
	 * Build a Museum from a string array
	 * @return The created Museums
	 **/
	private Musee tabToMusee(String[] tab)
	{
		Musee musee = null;
		try
		{
			musee = new Musee(tab[0], tab[1], Integer.parseInt(tab[2]), tab[3],
							  Integer.parseInt(tab[4]), tab[5].trim(),
							  Double.parseDouble(tab[6]),
							  Double.parseDouble(tab[7]), tab[8], tab[9],
							  tab[10], tab[11], tab[12]);
		}
		catch (Exception e)
		{
			System.out.print(
				"[CsvMuseeDao]\t A data entry is corrumpted, skip it :\n> Line\t --  ");
			for (String s : tab)
				System.out.print(s + "\t");
			System.out.print("\n");
			return null;
		}
		return musee;
	}

	private CsvFile csvFile; // The CSV File analyzed
}