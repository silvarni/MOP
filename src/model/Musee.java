package Model;
/**
 * Musee
 */
public class Musee extends PointInteret
{
	// --- public
	public Musee(String nom, String adresse, int code_postal, String commune,
				 int insee, String acces, double latitude, double longitude,
				 String telephone, String courriel, String site_web,
				 String facebook, String descriptif)
	{
		super(latitude, longitude, commune, insee, descriptif);
		m_nom		  = nom;
		m_adresse	 = adresse;
		m_code_postal = code_postal;
		m_acces		  = acces;
		m_telephone   = telephone;
		m_courriel	= courriel;
		m_site_web	= site_web;
		m_facebook	= facebook;
		m_descriptif  = descriptif;
		m_categorie   = "musee";
	}

	/// Builds a description from a Museum
	@Override public String get_description()
	{
		return "Nom\t\t " + m_nom + "\nCommune\t " + m_commune + " \nInsee\t " +
			m_insee + "\nPosition\t (" + m_position.m_latitude + "N, " +
			m_position.m_longitude + "W)"
			+ "\nAdresse:\t " + m_adresse + "\nCommune:\t " + m_commune +
			"\nCode Postale:\t " + m_code_postal + "\nAcces:\t\t " + m_acces +
			"\nTelephone:\t " + m_telephone + "\nCouriel:\t " + m_courriel +
			"\nSite:\t\t " + m_site_web + "\nFacebook:\t " + m_facebook +
			"\nDescriptiion :\n\t" + m_descriptif;
	}

	/// Builds a short description for a museum
	@Override public String toString()
	{
		return m_nom + " (" + m_insee + " " + m_commune + ")";
	}

	// --- private
	private String m_nom;
	private String m_adresse;
	private int	m_code_postal;
	private String m_acces;
	private String m_telephone;
	private String m_courriel;
	private String m_site_web;
	private String m_facebook;
}
