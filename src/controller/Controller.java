package Controller;

import java.awt.Rectangle;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

import View.*;
import Model.*;

/**
 * Only class managing whole application control
 **/
public class Controller implements ActionListener, MouseListener,
								   MouseMotionListener,
								   DocumentListener /*, ListSelectionListener*/,
								   TreeSelectionListener
{
	// --- Public
	public Controller(View view)
	{
		m_view = view;

		// Action
		Collection< AbstractButton > buttons =
			m_view.get_buttons().get_buttons();

		for (AbstractButton b : buttons)
			b.addActionListener(this);

		// Mouse
		m_view.get_map().addMouseListener(this);

		// Document
		m_view.get_search().get_bar().getDocument().addDocumentListener(this);

		// List
		// m_view.get_search().get_list().addListSelectionListener(this);
		m_view.get_search().get_tree().addTreeSelectionListener(this);
	}

	/**
	 * Manage button pressed event
	 **/
	@Override public void actionPerformed(ActionEvent ae)
	{
		Object source = ae.getSource();


		for (String name : Model.categories)
		{
			if (source == m_view.get_buttons().get_button(name))
			{
				AbstractButton button = (AbstractButton)source;
				m_view.get_search().set_category_active(name,
														button.isSelected());
				m_view.repaint();
				return;
			}
		}


		if (source == m_view.get_buttons().get_button("clear"))
		{
			m_view.get_search().clear();
		}
		else if (source == m_view.get_buttons().get_button("all"))
		{
			m_view.get_search().add_all();
		}
		m_view.repaint();
	}

	/**
	 * Manage mouse button clicked event
	 **/
	@Override public void mouseClicked(MouseEvent e)
	{
		// m_view.get_map().update_under_mouse(e.getX(), e.getY());
		// m_view.repaint();
	}

	/**
	 * Manage Mouse enter event
	 **/
	@Override public void mouseEntered(MouseEvent e) {}

	/**
	 * Manage Mouse exit event
	 **/
	@Override public void mouseExited(MouseEvent e) {}

	/**
	 * Manage mouse button pressed event
	 **/
	@Override public void mousePressed(MouseEvent e)
	{
		m_view.get_map().begin_selection(e.getX(), e.getY());
		m_view.get_map().addMouseMotionListener(this);
		m_view.get_map().update_clicked();
		m_view.get_map().repaint();
	}

	/**
	 * Manage mouse button released event
	 **/
	@Override public void mouseReleased(MouseEvent e)
	{
		m_view.get_map().end_selection(e.getX(), e.getY());
		m_view.get_map().removeMouseMotionListener(this);
	}

	/**
	 * Manage Mouse drag event
	 **/
	@Override public void mouseDragged(MouseEvent e)
	{
		Rectangle select = m_view.get_map().get_select();
		select.width	 = e.getX() - select.x;
		select.height	= e.getY() - select.y;
		m_view.get_map().update_clicked();
		m_view.get_map().repaint();
	}

	@Override public void mouseMoved(MouseEvent e) {}

	@Override public void changedUpdate(DocumentEvent e) {}

	/**
	 * Manage search bar insert event
	 **/
	@Override public void insertUpdate(DocumentEvent e)
	{
		if (e.getDocument() == m_view.get_search().get_bar().getDocument())
		{
			m_view.get_map().clear();
			m_view.get_search().update();
			m_view.repaint();
		}
	}

	/**
	* Manage search bar remove event
	**/
	@Override public void removeUpdate(DocumentEvent e) { insertUpdate(e); }

	/**
	 * Manage tree result event
	 **/
	@Override public void valueChanged(TreeSelectionEvent e)
	{
		// if (e.getSource() == m_view.get_search().get_list())
		// m_view.get_search().update_search();
		if (e.getSource() == m_view.get_search().get_tree())
		{
			m_view.get_search().update_search();
			m_view.repaint();
		}
	}

	// --- Protected
	View m_view;
}