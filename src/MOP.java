package Application;

import View.*;
import Controller.*;
import Model.*;

public class MOP
{
	public static void main(String args[])
	{
		System.out.println("[Application]\t --- Started.");
		Model model = new Model();
		View  view  = new View(model);
		Controller controller = new Controller(view);
		//System.out.println("[Application]\t --- Ended.");
	}
}
