package View;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.lang.reflect.Array;

import Model.*;
import View.*;

/**
 * Encapsulates the description of selected PointInteret into 15 tabs
 **/
public class Description extends JTabbedPane
{
	public static final int MAX_TAB_NUMBER = 15;
	public static final int MAX_TAB_LENGTH = 10;

	// --- Public
	public Description()
	{
		m_described		= new ArrayList< PointInteret >();
		m_others		= new JTextArea(10, 30);
		m_others_filled = false;

		setPreferredSize(
			new Dimension(View.WINDOW_WIDTH, View.WINDOW_HEIGHT / 3));
	}

	/// Updates the description to print all the @ref pis descriptions
	public void update(ArrayList< PointInteret > pis)
	{
		// m_described = (ArrayList<PointInteret>)pis.clone();
		m_described		= pis;
		m_others_filled = false;
		rewrite();
	}

	/// Adds a PointInteret to the list to describe
	public void add(PointInteret pi)
	{
		if (!m_described.contains(pi))
		{
			m_described.add(pi);
			if (add_tab(pi))
			{
				add("Others", new JScrollPane(m_others));
			}
		}
	}

	/// Not used : set only one direction : @ref pi 's
	public void focus(PointInteret pi)
	{
		m_described.clear();
		m_described.add(pi);
		rewrite();
	}

	/// Rebuilds all the tabs from the m_described array
	public void rewrite()
	{
		removeAll();
		String result = new String();

		for (PointInteret pi : m_described)
		{
			if (add_tab(pi))
			{
				add("Others", new JScrollPane(m_others));
			}
		}
	}

	/// Adds a tab describing the PointInteret @ref pi
	public boolean add_tab(PointInteret pi)
	{
		JTextArea text_area = new JTextArea(10, 30);
		text_area.setLineWrap(true);
		text_area.setEditable(false);
		text_area.setText(pi.get_description());
		boolean too_many = false;

		if (Array.getLength(getComponents()) < MAX_TAB_NUMBER)
		{
			String tab_name = pi.get_categorie();
			if (tab_name.length() > MAX_TAB_LENGTH)
			{
				tab_name = tab_name.substring(0, MAX_TAB_LENGTH - 3) + "...";
			}
			add(tab_name, new JScrollPane(text_area));
		}
		else
		{
			if (m_others_filled == false)
			{
				too_many		= true;
				m_others_filled = true;
			}
			m_others.append(pi.get_description() + "\n ----------- \n");
		}
		return too_many;
	}

	// --- Protected
	protected ArrayList< PointInteret >
			m_described;	 /// List of described PointInteret
	boolean m_others_filled; /// Used to know when to display a 'Others' tab if
							 /// too many results
	JTextArea m_others;		 /// If too many results, fill this JTextArea
}