package View;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import View.*;
import Model.*;

import java.util.*;

/**
 * TextField that allows the user to search with keyword
 **/
public class Search extends JPanel
{
	public Search(Model model, View view)
	{
		super(new BorderLayout());
		setPreferredSize(new Dimension(View.WINDOW_WIDTH / 3 * 2,
									   View.WINDOW_HEIGHT / 3 * 2));

		// Constructor
		m_model = model;
		m_view  = view;

		// Find all interests
		HashMap< String, ArrayList< PointInteret > > col =
			model.get_collection();
		m_interests = new ArrayList< PointInteret >();
		for (String category : Model.categories)
			m_interests.addAll(col.get(category));

		m_buttons			= new Buttons();
		m_active_categories = new ArrayList< String >();
		m_results			= new ArrayList< PointInteret >();

		// Member class
		m_bar		  = new JTextField(20);
		m_result_tree = new ResultTree();

		// Swing
		m_scroll_pane = new JScrollPane(m_result_tree.get_tree()); // m_list
		m_scroll_pane.setPreferredSize(
			new Dimension(View.WINDOW_WIDTH / 3, View.WINDOW_HEIGHT / 3));

		// Add everything
		JPanel search = new JPanel();
		search.add(new JLabel("Search"));
		search.add(m_bar);
		add(search, BorderLayout.PAGE_START);
		add(m_buttons, BorderLayout.LINE_START);
		add(m_scroll_pane, BorderLayout.LINE_END);

		// Default behavior
		add_all();
	}

	// Update the results using the current text
	public void update()
	{
		m_results.clear();
		ArrayList< PointInteret > results =
			m_model.get_monument_dao().find_by_keyword(m_bar.getText());
		results.addAll(
			m_model.get_musee_dao().find_by_keyword(m_bar.getText()));

		for (PointInteret pi : results)
		{
			if (m_active_categories.contains(pi.get_categorie()))
			{
				m_results.add(pi);
			}
		}

		Collections.sort(m_results);
		// m_list.setListData(m_results.toArray());
		m_result_tree.rebuild_from(m_results);

		m_view.get_map().to_print = m_result_tree.get_all_leaves();
	}

	/// Tells the other classes to update with the current results
	public void update_search()
	{
		TreePath[] selected_path = m_result_tree.get_tree().getSelectionPaths();
		if (selected_path == null)
			selected_path = new TreePath[0];
		HashMap< TreePath, PointInteret > leaves = m_result_tree.get_leaves();
		ArrayList< PointInteret > selected = new ArrayList< PointInteret >();
		for (TreePath path : selected_path)
		{
			if (m_result_tree.is_leaf(path))
				selected.add(leaves.get(path));
			else
				selected.addAll(m_result_tree.get_all_leaves_from_node(path));
		}
		m_view.get_map().selected = selected;
		m_view.get_map().to_print = m_result_tree.get_all_leaves();
		m_view.get_description().update(selected);
	}

	/// Allows a category to be displayed, or not
	public void set_category_active(String key, boolean active)
	{
		if (active)
		{
			m_active_categories.add(key);
		}
		else
		{
			m_active_categories.remove(key);
		}
		update();
		System.out.println("[MAP] Set category " + key + " activated to : " +
						   active);
	}

	public ArrayList< String > get_active_categories()
	{
		return m_active_categories;
	}

	public void clear()
	{
		m_view.get_map().selected.clear();
		m_view.get_map().to_print.clear();
		m_active_categories.clear();
		m_buttons.set_checked(false);
		m_result_tree.clear();
		m_bar.setText("");
	}

	/// Add all possible result to the result
	public void add_all()
	{
		m_active_categories = (ArrayList< String >)(Model.categories.clone());
		m_view.get_map().to_print =
			(ArrayList< PointInteret >)(m_interests.clone());
		m_result_tree.rebuild_from(m_interests);
		m_buttons.set_checked(true);
		m_bar.setText("");
	}

	public JTree	  get_tree() { return m_result_tree.get_tree(); }
	public JTextField get_bar() { return m_bar; }
	public Buttons	get_buttons() { return m_buttons; }


	// --- PROTECTED
	// Member
	Model								m_model;
	View								m_view;
	protected ArrayList< PointInteret > m_interests;

	// Swing
	JTextField  m_bar;
	JScrollPane m_scroll_pane;

	// Search
	ArrayList< PointInteret >	 m_results;
	protected ArrayList< String > m_active_categories;

	// Result Tree
	protected ResultTree m_result_tree;

	// Buttons
	protected Buttons m_buttons;
}