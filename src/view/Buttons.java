package View;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import Model.*;

/**
 * The class encapsuling the CheckBoxes
 **/
public class Buttons extends JPanel
{
	// --- Public
	public Buttons()
	{
		super();
		m_grid = new JPanel(new GridLayout(6, 3, 0, 0));
		m_grid.setPreferredSize(
			new Dimension(View.WINDOW_WIDTH / 3, View.WINDOW_HEIGHT / 2));
		m_bottom = new JPanel();

		setPreferredSize(
			new Dimension(View.WINDOW_WIDTH / 3, View.WINDOW_HEIGHT / 3));

		// Init buttons
		m_buttons = new HashMap< String, AbstractButton >();
		for (String button_name : Model.categories)
		{
			AbstractButton b = new JCheckBox(
				"<html>" + button_name +
				"</html>"); // new ImageIcon(m_map.get_icon(button_name)
			m_buttons.put(button_name, b);
			m_grid.add(b);
		}

		// Special buttons
		m_buttons.put("clear", new JButton("clear"));
		m_bottom.add(m_buttons.get("clear"));

		m_buttons.put("all", new JButton("all"));
		m_bottom.add(m_buttons.get("all"));

		add(m_grid);
		add(m_bottom);

		// init
		set_checked(true);
	}

	public AbstractButton get_button(String key) { return m_buttons.get(key); }

	public Collection< AbstractButton > get_buttons()
	{
		return m_buttons.values();
	}

	public void set_checked(boolean b)
	{
		for (AbstractButton bt : m_buttons.values())
			bt.setSelected(b);
	}

	// --- Protected
	protected JPanel m_grid;
	protected JPanel m_bottom;
	protected HashMap< String, AbstractButton > m_buttons;
}