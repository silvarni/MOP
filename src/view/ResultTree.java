package View;

import Model.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.util.*;

/**
 * Displays all the search result in a JTree sorted by category
 **/
public class ResultTree
{
	// --- PUBLIC
	public ResultTree()
	{
		m_root			= new DefaultMutableTreeNode("Search Results");
		m_tree_model	= new DefaultTreeModel(m_root);
		m_tree			= new JTree(m_tree_model);
		m_branches		= new HashMap< String, DefaultMutableTreeNode >();
		m_branches_path = new HashMap< TreePath, DefaultMutableTreeNode >();
		m_leaves		= new HashMap< TreePath, PointInteret >();

		m_branches.put("root", m_root);
	}

	/// Easy way to add a branch
	public void add_branch(String top, String name)
	{
		if (m_branches.containsKey(top))
		{
			DefaultMutableTreeNode new_branch =
				new DefaultMutableTreeNode(name);
			m_branches.get(top).add(new_branch); // Add to JTree
			m_branches.put(name, new_branch);	// Add to HashMap
			m_branches_path.put(new TreePath(new_branch.getPath()), new_branch);
		}
		else
			throw new IllegalArgumentException("[JTree]\t Cannot find node " +
											   top + ".");
	}

	/// Easy way to add a leaf
	public void add_leaf(String top, PointInteret pi)
	{
		if (m_branches.containsKey(top))
		{
			DefaultMutableTreeNode new_node = new DefaultMutableTreeNode(pi);

			m_branches.get(top).add(new_node);
			m_leaves.put(new TreePath(new_node.getPath()), pi);
			// System.out.println("[JTree]\t Added leaf " + pi + " to branch " +
			// top +".");
		}
		else
			throw new IllegalArgumentException("[JTree]\t Cannot find node " +
											   top + ".");
	}

	public DefaultMutableTreeNode get_branch(String key)
	{
		if (m_branches.containsKey(key))
			return m_branches.get(key);
		return null;
	}

	public boolean has_branch(String key)
	{
		return m_branches.containsKey(key);
	}

	/// Totally rebuild the JTree from a list of PointInteret
	public void rebuild_from(ArrayList< PointInteret > results)
	{
		// Clear
		m_branches.clear();
		m_leaves.clear();

		// build root
		m_root.removeAllChildren();
		m_branches.put("root", m_root);


		// Add results
		for (PointInteret pi : results)
		{
			if (pi == null)
				continue;

			if (!has_branch(pi.get_categorie()))
				add_branch("root", pi.get_categorie());

			add_leaf(pi.get_categorie(), pi);
		}

		// Auto expand
		m_tree_model.reload();

		if (m_leaves.size() < 15)
		{
			Set< TreePath > branches = m_branches_path.keySet();
			for (TreePath path : branches)
			{
				m_tree.expandPath(path);
			}
		}
	}

	public ArrayList< PointInteret > get_all_leaves()
	{
		ArrayList< PointInteret > res = new ArrayList< PointInteret >();
		res.addAll(m_leaves.values());
		return res;
	}

	/// Returns all the leaves of a subtree
	public ArrayList< PointInteret > get_all_leaves_from_node(TreePath path)
	{
		ArrayList< PointInteret > result = new ArrayList< PointInteret >();
		if (m_branches_path.containsKey(path))
		{
			// System.out.println("is branch.");
			DefaultMutableTreeNode node		= m_branches_path.get(path);
			Enumeration			   children = node.children();
			while (children.hasMoreElements())
			{
				DefaultMutableTreeNode child =
					(DefaultMutableTreeNode)children.nextElement();
				if (m_leaves.containsKey(child.getPath()))
					result.add(m_leaves.get(child.getPath()));
				else
					result.addAll(get_all_leaves_from_node(
						new TreePath(child.getPath())));
			}
		}
		else if (is_leaf(path))
		{
			// System.out.println("is leave.");
			result.add(m_leaves.get(path));
		}
		return result;
	}

	public boolean is_leaf(TreePath p)
	{
		// System.out.println("Considering path :" + p + "\n with keyset : " +
		// m_leaves.keySet() );
		return m_leaves.keySet().contains(p);
	}

	/// Empty the JTree
	public void clear()
	{
		m_leaves.clear();
		m_branches.clear();
		m_root.removeAllChildren();
		m_tree_model.reload();
	}

	public JTree get_tree() { return m_tree; }
	public HashMap< TreePath, PointInteret > get_leaves() { return m_leaves; }

	// --- PROTECTED
	protected DefaultMutableTreeNode m_root; /// JTree root
	protected HashMap< String, DefaultMutableTreeNode >   m_branches;
	protected HashMap< TreePath, DefaultMutableTreeNode > m_branches_path;
	protected HashMap< TreePath, PointInteret >			  m_leaves;
	protected JTree			   m_tree;
	protected DefaultTreeModel m_tree_model;
}