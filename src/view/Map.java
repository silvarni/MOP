package View;

import javax.imageio.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.util.*;

import Model.*;
import Controller.*;

/**
 * Manages all the work to display the map (including pins)
 **/
public class Map extends JPanel
{
	// --- Public
	public Map(View view)
	{
		super();
		setPreferredSize(
			new Dimension(View.WINDOW_WIDTH / 3, View.WINDOW_HEIGHT / 3));

		m_view = view;

		// --- Init map
		try
		{
			m_map = ImageIO.read(
				this.getClass().getResource("/assets/img/map.jpg"));
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		// --- PointInteret Management
		selected = new ArrayList< PointInteret >();
		to_print = new ArrayList< PointInteret >();

		// --- Set default image icon
		Image im_def = null;
		try
		{
			im_def = ImageIO.read(
				this.getClass().getResource("/assets/img/default.png"));
		}
		catch (IOException e)
		{
			System.out.println("[Map]\t Can't use a default image :" +
							   e.getMessage());
		}

		// --- Initialize image icons
		m_icons = new HashMap< String, Image >();
		for (String image_name : Model.categories)
		{
			Image im = null;
			try
			{
				im = ImageIO.read(this.getClass().getResource(
					"/assets/img/" + image_name + ".png"));
			}
			catch (IOException e)
			{
				System.out.print("[Map] IOException: Image_name : " +
								 image_name + " : " + e.getMessage());
				// e.printStackTrace();
				if (im_def != null)
				{
					System.out.println("\n\t-> Use default image icon");
					m_icons.put(image_name, im_def);
				}
				else
					System.out.println(
						"  -> Skiped because there is no default image icon.");
				continue;
			}
			catch (IllegalArgumentException e)
			{
				System.out.println("[MAP] \"/assets/img/" + image_name +
								   ".png\" not found; use default");
				m_icons.put(image_name, im_def);
				continue;
			}

			m_icons.put(image_name, im);
			System.out.println("[MAP] Added " + image_name + " icon.");
		}

		clear();
		// --- Add everything
		// add(new JLabel(new ImageIcon(m_map)));
	}

	/// Starts Rectangular selection
	public void begin_selection(int x, int y)
	{
		selection		= true;
		m_select.x		= x;
		m_select.y		= y;
		m_select.width  = 1;
		m_select.height = 1;
	}

	/// Ends Rectangular selection
	public void end_selection(int x, int y)
	{
		selection		= false;
		m_select.width  = x;
		m_select.height = y;
	}

	@Override public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		// System.out.println(m_active_categories);
		g.drawImage(m_map, 0, 0, 381, 516, null);

		// Draw selected
		ArrayList< PointInteret > to_draw =
			(ArrayList< PointInteret >)to_print.clone();
		to_draw.addAll(selected);
		for (PointInteret pi : to_draw)
		{
			Position location = m_view.mapCoordToPixel(
				pi.get_position().m_longitude, pi.get_position().m_latitude);
			g.drawImage(m_icons.get(pi.get_categorie()),
						(int)location.m_longitude, (int)location.m_latitude,
						null);
			if (selected.contains(pi))
				g.drawRect((int)location.m_longitude, (int)location.m_latitude,
						   View.ICON_SIZE, View.ICON_SIZE);
		}

		// Draw selection
		g.drawRect(m_select.x, m_select.y, m_select.width, m_select.height);
	}

	/// Determines all the mouse selected PointInteret
	public void update_clicked()
	{
		ArrayList< PointInteret > clicked = new ArrayList< PointInteret >();
		for (PointInteret pi : to_print)
		{
			Position location = m_view.mapCoordToPixel(
				pi.get_position().m_longitude, pi.get_position().m_latitude);

			// Position mouse = new Position(y, x);
			// double   distance =
			// 	Math.abs(mouse.m_longitude - location.m_longitude) +
			// 	Math.abs(mouse.m_latitude - location.m_latitude);
			// System.out.println("Mouse : " + mouse + " location " + location +
			// " distance " + distance);

			// if (distance < View.ICON_SIZE)
			Rectangle icon = new Rectangle((int)location.m_longitude,
										   (int)location.m_latitude,
										   View.ICON_SIZE, View.ICON_SIZE);
			if (m_select.intersects(icon))
			{
				clicked.add(pi);
				if (!selected.contains(pi))
					selected.add(pi);
			}
			else
			{
				if (selected.contains(pi))
					selected.remove(pi);
			}
		}
		m_view.get_description().update(clicked);
	}

	public Image get_icon(String category) { return m_icons.get(category); }
	public Rectangle			 get_select() { return m_select; }

	/// Clear the contents to draw : no more pins would be drawn
	public void clear()
	{
		selected.clear();
		to_print.clear();
		m_select = new Rectangle(0, 0, m_map.getWidth(), m_map.getHeight());
	}

	public ArrayList< PointInteret > selected; /// Those that will be squared

	/// Those that will be displayed / painted
	public ArrayList< PointInteret > to_print;


	// --- Protected
	protected BufferedImage m_map;				// The sprite
	protected HashMap< String, Image > m_icons; // The icons references by name
	protected View m_view;						// The View = the container

	// --- Private
	private boolean   selection = false; /// Are we selecting with the mouse ?
	private Rectangle m_select;			 /// The rectangular selection
}