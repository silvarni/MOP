package View;

import javax.swing.*;
import java.awt.*;
import java.util.*;


import Model.*;


/**
 * Main class containing all GUI elements
 **/
public class View
{
	// --- Public
	/// Hard code of icon size
	public static final int ICON_SIZE = 16; // px

	/// Hard coded window width
	public static final int WINDOW_WIDTH = 1200;

	/// Hard coded window height
	public static final int WINDOW_HEIGHT = 800;

	/// Top left Coordinates of the map sprite
	public static final Position TOP_LEFT_CORNER_COORDINATES =
		new Position(48.183, 5.25); // 48.183N 5.25W

	/// Bottom right Coordinates of the map sprite
	public static final Position BOTTOM_RIGHT_CORNER_COORDINATES =
		new Position(46.23, 7.23); // 46.23N 7.23W


	/// Changes a Degree coordinates to pixel
	public static Position mapCoordToPixel(double longitude, double latitude)
	{
		double width  = 381;
		double height = 516;

		Position res = new Position(
			height * (TOP_LEFT_CORNER_COORDINATES.m_latitude - latitude) /
				(TOP_LEFT_CORNER_COORDINATES.m_latitude -
				 BOTTOM_RIGHT_CORNER_COORDINATES.m_latitude),
			width * (longitude - TOP_LEFT_CORNER_COORDINATES.m_longitude) /
				(BOTTOM_RIGHT_CORNER_COORDINATES.m_longitude -
				 TOP_LEFT_CORNER_COORDINATES.m_longitude));
		return res;
	}

	public View(Model model)
	{
		m_title = "Franche-Comte";

		// --- Init window
		m_window = new JFrame(m_title);
		m_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// --- Init map
		m_map = new Map(this);

		// --- Init Description
		m_description = new Description();

		// --- Init Search
		m_search = new Search(model, this);


		// --- Add everything to window
		JScrollPane left = new JScrollPane(m_search);
		m_window.add(left, BorderLayout.LINE_START);
		m_window.add(m_map, BorderLayout.CENTER);
		m_window.add(m_description, BorderLayout.PAGE_END);


		// --- /!\ Last
		m_window.setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		m_window.setVisible(true);
		m_window.setResizable(false);
	}

	// Getters
	public Buttons	 get_buttons() { return m_search.get_buttons(); }
	public Map		   get_map() { return m_map; }
	public Description get_description() { return m_description; }
	public Search	  get_search() { return m_search; }

	public void repaint() { m_window.repaint(); }

	// --- Protected
	// Window
	protected String m_title; /// Window title
	protected JFrame m_window;

	// Map
	protected Map m_map;

	// Description
	protected Description m_description;

	// Search
	protected Search m_search;
}